<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Activity;
use \App\User;
use Auth;
use Session;

class ActivityController extends Controller
{
    public function index(){
    	$activities = Activity::all();
    	return view('bulletin', compact('activities'));
    }

    public function createActivity(){
    	$activities = Activity::all();

    	return view('adminviews.addactivity', compact('activities'));
    }

    public function storeActivity(Request $req){
    	$rules = array(
    		"title" => "required",
    		"description" => "required",
    		"date" => "required"
    	);

    	$this->validate($req, $rules);

    	// capture
    	$activity = new Activity;
    	$activity->title = $req->title;
    	$activity->description = $req->description;
    	$activity->date = $req->date;
    	$activity->save();

    	Session::flash("message", "$activity->title has been added");
    	return redirect()->back();
    }

    public function createAttendance(){
        $activities = Activity::all();

        return view('userviews.createattendance', compact('activities'));
    }

    public function storeAttendance(Request $req){
        $user = Auth::user()->id;
        $activities = Activity::find($req->input('activity_id'));
        $activities->users()->attach($user);
        $activities->save();

        return redirect('/bulletin');
    }

    

    public function editActivity($id){
    	$activity = Activity::find($id);    	

    	return view('adminviews.editactivity', compact('activity'));
    }

    public function updateActivity($id, Request $req){
        $activity = Activity::find($id);

        $rules = array(
            "title" => "required",
            "description" => "required",
            "date" => "required"
        );

        $this->validate($req, $rules);

        $activity->title = $req->title;
        $activity->description = $req->description;
        $activity->date = $req->date;

        $activity->save();
        Session::flash('message', "$activity->name has been updated");
        return redirect('/bulletin');
    }

    public function showAttendees($id){
        $activities = Activity::where('id', $id)->get();
        
        return view('adminviews.activity_attendance', compact('activities'));
    }

    public function viewMyRecord(){
        $users = User::where('id', Auth::user()->id)->get();
        
        return view('userviews.myrecord', compact('users'));
    }
}
