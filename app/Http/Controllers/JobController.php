<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Job;
use Session;

class JobController extends Controller
{
    public function createJob(){
    	$jobs = Job::all();

    	return view('adminviews.addjob', compact('jobs'));
    }

    public function storeJob(Request $req){
    	$rules = array(
    		"title" => "required",
    		"description" => "required"
    	);

    	$this->validate($req, $rules);

    	// capture
    	$job = new Job;
    	$job->title = $req->title;
    	$job->description = $req->description;
    	$job->save();

    	Session::flash("message", "$job->title has been added");
    	return redirect()->back();
    }
}
