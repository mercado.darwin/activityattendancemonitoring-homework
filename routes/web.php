<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/bulletin', 'ActivityController@index');

Route::get('/addjob', 'JobController@createJob');
Route::post('/addjob', 'JobController@storeJob');

Route::get('/addactivity', 'ActivityController@createActivity');
Route::post('/addactivity', 'ActivityController@storeActivity');

Route::get('/createattendance', 'ActivityController@createAttendance');
Route::post('/createattendance', 'ActivityController@storeAttendance');

Route::get('/editactivity/{id}', 'ActivityController@editActivity');
Route::patch('/editactivity/{id}', 'ActivityController@updateActivity');

Route::get('/myrecord', 'ActivityController@viewMyRecord');

Route::get('/showattendees/{id}', 'ActivityController@showAttendees');

Route::get('/home', 'HomeController@index')->name('home');
