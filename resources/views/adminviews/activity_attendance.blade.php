@extends('layouts.app')
@section('content')


<div class="container d-flex justify-content-center">
	<div class="row">
		<div class="">
			<div class="row w-100">
				<div class="col-lg-12 p-3 my-2">
					<div class="card">
						<div class="card-body text-center">
							@foreach($activities as $activity)
							<tr>
								<h1>{{$activity->title}}<br></h1>					
								@foreach($activity->users as $user)
									<td>{{$user->name}}<br></td>
								@endforeach
							</tr>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

@endsection